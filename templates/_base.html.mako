<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>bntools/${SITE_NAME}${self.title()}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link
            href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
            rel="stylesheet"
            integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
            crossorigin="anonymous"
        />
        <link href="${BASE_URL}/style.css" rel="stylesheet" />
    </head>
    <body>
        <script src="/common/langselect.js"></script>
        <div class="pb-5">
            ${self.body()}
        </div>
    </body>
</html>
